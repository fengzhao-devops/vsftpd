[https://funzzz.fun/2021/02/04/vsftpd/](https://funzzz.fun/2021/02/04/vsftpd/)

[https://blog.creativecc.cn/posts/Network-Vsftpd.html](https://blog.creativecc.cn/posts/Network-Vsftpd.html)

FTP是应用层的一个文件传输协议，使用了两个并行的TCP来传输文件，一个是控制连接(21)一个是数据连接(20)

在介绍FTP的工作原理时候，通常会讲到两个信道(控制信道、数据信道)和两种模式(主动模式、被动模式)

控制信道和数据信道，可以初步的理解为

- 控制信道是创建会话的必要条件,通常传输控制信息，如口令，用户标识，存放、获取文件等命令。
- 数据信道则是临时创立的，它通常被用来传输文件。当发送完文件之后数据连接会自动关闭。

至于两种模式，主动和被动可以站在服务器的角度去理解：我(服务器)主动发起请求，我就是主动的，反之同理。举例：

- 主动模式：
    - 控制信道：客户端(端口随机)  -----> 服务器21端口
    - 数据信道：客户端(端口随机)  <----- 服务器20端口
    

在主动模式下，客户端向服务器的FTP端口21发起连接请求，服务器接收连接后，创建一条命令链路。

当需要传输数据时，客户端在链路上用PORT命令告诉服务器:"我打开了X端口，需要你连接我"。

接着服务器从20端口向客户端的X端口发起连接请求，之后双方可以利用数据通道来传输数据

- 被动模式:
    - 控制信道：客户端(端口随机)  -----> 服务器21端口
    - 数据信道：客户端(端口随机)  <----- 服务器随机端口

被动模式下控制信道的传输方式和主动一致，但是在需要数据传输的时候，服务器在控制信道上用PASV的命令告诉客户端，我开启了XX端口，你过来连接我，之后服务器被动等待客户端连接它的XX端口，创建连接后开始传输数据

在创建连接的过程中，有很多FTP独有的命令。例如在被动模式下的控制信道，经过三次握手后，客户端会向服务器发送PASV命令，意为：你是否支持被动模式，服务器如果支持则会返回227:a,b,c,d,e,f。其中a,b,c,d为IP地址。端口为e*256+f，到了第二信道，双方的通信端口都是随机的，这样较为安全。

**1.1 FTP主动和被动工作模式**

1）port方式：主动模式

port（主动）方式的连接过程是：客户端向服务器的FTP端口（默认是21）发送连接请求，服务器接受连接，建立一条命令链路。当需要传送数据时，服务器从20端口向客户端的空闲端口发送连接请求，建立一条数据链路来传送数据。

2）pasv方式：被动模式

pasv（被动）方式的连接过程是：客户端向服务器的FTP端口（默认是21）发送连接请求，服务器接受连接，建立一条命令链路。当需要传送数据时，客户端向服务器的空闲端口发送连接请求，建立一条数据链路来传送数据。

**1.2 vsftp用户认证类型**

- 匿名登录：在登录FTP时使用默认的用户名，一般是ftp或anonymous。任何人都可以无需密码验证而直接登录到FTP服务器。
- 本地用户登录：使用ftp服务器的操作系统用户登录，在/etc/passwd中。
- 虚拟用户登录：这是FTP专有用户，有两种方式实现虚拟用户，本地数据文件和数据库服务器（MySQL）。

本地用户是指ftp的用户认证体系跟操作系统的认证体系是用的同一套认证体系，较为不安全，一旦设置不好，可能会造成让ftp用户也登录到ftp服务器操作系统中。

虚拟用户是指ftp的用户认证体系是自己的单独认证体系。

FTP虚拟用户是FTP服务器的专有用户，使用虚拟用户账号可以提供集中管理的FTP根目录，方便了管理员的管理，同时将用于FTP登录的用户名、密码与系统用户账号区别开，进一步增强了FTP服务器的安全性。

某种意义上来说，匿名用户也是系统用户，只是系统用户的一个映射。公开的FTP都不会使用系统用户作为FTP帐号，而更多的采用了虚拟用户，这样能保证系统的安全性。

使用虚拟用户登录FTP服务器，可以避免使用操作系统帐号作为FTP用户带来的一些安全问题，也便于通过数据库或其它程序来进行管理。

虚拟用户的特点是只能访问服务器为其提供的FTP服务，而不能访问系统的其它资源。所以，如果想让用户对FTP服务器站内具有写权限，但又不允许访问系统其它资源，可以使用虚拟用户来提高系统的安全性。

在VSFTP中，认证这些虚拟用户使用的是单独的口令库文件（pam_userdb），由可插入认证模块（PAM）认证。使用这种方式更加安全，并且配置更加灵活。

基本流程：FTP用户访问->PAM配置文件（由vsftpd.conf中pam_service_name指定）->PAM论证->区别用户读取配置文件（由vsftpd.conf中user_config_dir指定配置文件路径，文件名即用户名）

vsftpd简介

**vsftpd** 是“**very secure FTP daemon**”的缩写，**安全性**是它的一个最大的特点。**vsftpd** 是一个运行在类 **UNIX 类操作系统**上运行的ftp server软件，它可以运行在诸如 **Linux**、**BSD**、**Solaris**、HP-UNIX**等系统上面。**

它**是一个**完全免费的、开放源代码的ftp服务器软件，支持很多其他的 FTP 服务器所不支持的特征。比如：**非常高的安全性需求、带宽限制、良好的可伸缩性、可创建虚拟用户、支持IPv6、速率高**等。

**vsftpd**是一款在**Linux发行版**中最受推崇的**FTP服务器程序**。**特点**是**小巧轻快，安全易用**。

```bash

wget https://security.appspot.com/downloads/vsftpd-3.0.5.tar.gz

tar -zxvf vsftpd-3.0.5.tar.gz

cd vsftpd-3.0.5

# 构建， builddefs.h文件中可以修改构建时配置
# 编译构建成功后，会在当前目录下生成一个vsftpd的可执行文件
make 

find / -name "*libcap.so*"

ln -sv /usr/lib64/libcap.so.2 /usr/lib/libcap.so

# 匿名访问

```

```bash

yum install vsftpd

apt-get install vsftpd

```

```bash
#与主机较相关的设定值
connect_from_port_20=YES (NO)

#ftp-data 的端口
listen_port=21

#当用户进入某个目录时，会显示该目录需要注意的内容，显示的文件默认是 .message ，你可以使用底下的设定项目来修改！
dirmessage_enable=YES (NO)

#当 dirmessage_enable=YES 时，可以设定这个项目来让 vsftpd 寻找该文件来显示信息！
message_file=.message

# 若设定为 YES 表示 vsftpd 是以 standalone 的方式来启动的
listen=YES (NO)

#启动被动式联机模式(passive mode)，一定要设定为 YES 的
pasv_enable=YES (NO)

#是否使用本地时间？vsftpd 预设使用 GMT 时间(格林威治)
use_localtime=YES (NO)

# 如果你允许用户上传数据时，就要启动这个设定值
write_enable=YES (NO)

#单位是秒，在数据连接的主动式联机模式下，我们发出的连接讯号在 60 秒内得不到客户端的响应，则不等待并强制断线
connect_timeout=60

#当用户以被动式 PASV 来进行数据传输时，如果主机启用 passive port 并等待 client 超过 60 秒而无回应， 那么就给他强制断线！这个设定值与 connect_timeout 类似，不过一个是管理主动联机，一个管理被动联机
accept_timeout=60

#如果服务器与客户端的数据联机已经成功建立 (不论主动还是被动联机)，但是可能由于线路问题导致 300 秒内还是无法顺利的完成数据的传送，那客户端的联机就会被我们的 vsftpd 强制剔除！
data_connection_timeout=300

#如果使用者在 300 秒内都没有命令动作，强制脱机
idle_session_timeout=300

#如果 vsftpd 是以 stand alone 方式启动的，那么这个设定项目可以设定同一时间，最多有多少 client 可以同时连上 vsftpd
max_clients=0

#与上面 max_clients 类似，这里是同一个 IP 同一时间可允许多少联机？
max_per_ip=0

#上面两个是与 passive mode 使用的 port number 有关，如果您想要使用 65400 到 65410 这 11 个 port 来进行被动式联机模式的连接，可以这样设定 pasv_max_port=65410 以及 pasv_min_port=65400。 如果是 0 的话，表示随机取用而不限制
pasv_min_port=0, pasv_max_port=0

#当使用者联机进入到 vsftpd 时，在 FTP 客户端软件上头会显示的说明文字。不过，这个设定值数据比较少啦！ 建议你可以使用底下的设定值来取代这个项目
ftpd_banner=一些文字说明

#这个项目可以指定某个纯文本档作为使用者登入 vsftpd 服务器时所显示的欢迎字眼。
banner_file=/path/file

#           与实体用户较相关的设定值
#若这个值设定为 YES 时，那么任何非 anonymous 登入的账号，均会被假设成为 guest (访客)   至于访客在 vsftpd 当中，预设会取得 ftp 这个使用者的相关权限。但可以透过 guest_username 来修改
guest_enable=YES (NO)

#在 guest_enable=YES 时才会生效，指定访客的身份而已
guest_username=ftp

#这个设定值必须要为 YES 时，在 /etc/passwd 内的账号才能以实体用户的方式登入我们的 vsftpd 主机
local_enable=YES (NO)

#实体用户的传输速度限制，单位为 bytes/second， 0 为不限制
local_max_rate=0

#将用户限制在自己的家目录之内(chroot)！这个设定在 vsftpd 当中预设是 NO，因为有底下两个设定项目的辅助喔！ 所以不需要启动他！
chroot_local_user=YES (NO)

#是否启用将某些实体用户限制在他们的家目录内？预设是 NO ，不过，如果您想要让某些使用者无法离开他们的家目录时， 可以考虑将这个设定为 YES ，并且规划下个设定值
chroot_list_enable=YES (NO)

# 如果 chroot_list_enable=YES 那么就可以设定这个项目了！ 他里面可以规定那一个实体用户会被限制在自己的家目录内而无法离开！(chroot) 一行一个账号即可！
chroot_list_file=/etc/vsftpd.chroot_list

#是否藉助 vsftpd 的抵挡机制来处理某些不受欢迎的账号，与底下的设定有关；
userlist_enable=YES (NO)

#当 userlist_enable=YES 时才会生效的设定，若此设定值为 YES 时，则当使用者账号被列入到某个文件时， 在该文件内的使用者将无法登入 vsftpd 服务器！该文件名与下列设定项目有关。
userlist_deny=YES (NO)

#若上面 userlist_deny=YES 时，则这个文件就有用处了！在这个文件内的账号都无法使用 vsftpd
userlist_file=/etc/vsftpd.user_list

#                  匿名者登入的设定值
#设定为允许 anonymous 登入我们的 vsftpd 主机！预设是 YES ，底下的所有相关设定都需要将这个设定为 anonymous_enable=YES 之后才会生效！
anonymous_enable=YES (NO)

#仅允许 anonymous 具有下载可读文件的权限，预设是 YES
anon_world_readable_only=YES (NO)

#是否允许 anonymous 具有写入的权限？预设是 NO！如果要设定为 YES， 那么开放给 anonymous 写入的目录亦需要调整权限，让 vsftpd 的 PID 拥有者可以写入才行！
anon_other_write_enable=YES (NO)

#是否让 anonymous 具有建立目录的权限？默认值是 NO！如果要设定为 YES， 那么 anony_other_write_enable 必须设定为 YES ！
anon_mkdir_write_enable=YES (NO)

#是否让 anonymous 具有上传数据的功能，默认是 NO，如果要设定为 YES ， 则 anon_other_write_enable=YES 必须设定。
anon_upload_enable=YES (NO)

#将某些特殊的 email address 抵挡住，不让那些 anonymous 登入！ 如果以 anonymous 登入主机时，不是会要求输入密码吗？密码不是要您 输入您的 email address 吗？如果你很讨厌某些 email address ， 就可以使用这个设定来将他取消登入的权限！需与下个设定项目配合
deny_email_enable=YES (NO)

#如果 deny_email_enable=YES 时，可以利用这个设定项目来规定哪个 email address 不可登入我们的 vsftpd 喔！在上面设定的文件内，一行输入一个 email address 即可！
banned_email_file=/etc/vsftpd.banned_emails

#当设定为 YES 时，表示 anonymous 将会略过密码检验步骤，而直接进入 vsftpd 服务器内喔！所以一般预设都是 NO
no_anon_password=YES (NO)

#这个设定值后面接的数值单位为 bytes/秒 ，限制 anonymous 的传输速度，如果是 0 则不限制(由最大带宽所限制)，如果您想让 anonymous 仅有 30 KB/s 的速度，可以设定『anon_max_rate=30000』
anon_max_rate=0

#限制 anonymous 的权限！如果是 077 则 anonymous 传送过来的文件 权限会是 -rw-------
anon_umask=077

#                 关于系统安全方面的一些设定值
#如果设定为 YES ，那么 client 就可以使用 ASCII 格式下载文件
ascii_download_enable=YES (NO)

#与上一个设定类似的，只是这个设定针对上传而言！预设是 NO
ascii_upload_enable=YES (NO)

# 这个设定项目比较危险一点～当设定为 YES 时，表示每个建立的联机 都会拥有一支 process 在负责，可以增加 vsftpd 的效能。不过， 除非您的系统比较安全，而且硬件配备比较高，否则容易耗尽系统资源  一般建议设定为 NO
one_process_model=YES (NO)

#当然我们都习惯支持 TCP Wrappers 的  所以设定为 YES
tcp_wrappers=YES (NO)

#当设定为 YES 时，使用者上传与下载文件都会被纪录起来。记录的文件与下一个设定项目有关
xferlog_enable=YES (NO)

#如果上一个 xferlog_enable=YES 的话，这里就可以设定了！这个是登文件的名称
xferlog_file=/var/log/vsftpd.log

#是否设定为 wu ftp 相同的登录档格式？！预设为 NO ，因为登录档会比较容易读！ 不过，如果您有使用 wu ftp 登录文件的分析软件，这里才需要设定为 YES
xferlog_std_format=YES (NO)

#我们的 vsftpd 预设以 nobody 作为此一服务执行者的权限。因为 nobody 的权限 相当的低，因此即使被入侵，入侵者仅能取得 nobody 的权限
nopriv_user=nobody

#这个是 pam 模块的名称，我们放置在 /etc/pam.d/vsftpd 即是这个
pam_service_name=vsftpd
```
